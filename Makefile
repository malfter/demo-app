# Used by `image`, `push` & `deploy` targets, override as required
IMAGE_REG ?= registry.gitlab.com
IMAGE_REPO ?= malfter/demo-app
IMAGE_TAG ?= latest
CI_IMAGE_TAG ?= replace_me

IMAGE_ARG_BUILD_DATE ?= $(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
IMAGE_ARG_GIT_COMMIT ?= $(shell git rev-parse --short HEAD)

# Used by `test-api` target
TEST_HOST ?= localhost:8080

# Don't change
SRC_DIR := src

.PHONY: help local lint lint-fix build pull ci-tag-image scan tag-latest push push-latest run clean test-api .EXPORT_ALL_VARIABLES
.DEFAULT_GOAL := help

help:  ## 💬 This help message
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

local:  ## ☸️  Create local kubernetes environment with k3d
	k3d cluster create devCluster

lint: venv  ## 🔎 Lint & format, will not fix but sets exit code on error
	. $(SRC_DIR)/.venv/bin/activate \
	&& black --check $(SRC_DIR) \
	&& flake8 src/app/ && flake8 src/run.py

lint-fix: venv  ## 📜 Lint & format, will try to fix errors and modify code
	. $(SRC_DIR)/.venv/bin/activate \
	&& black $(SRC_DIR)

build:  ## 🔨 Build container image from Dockerfile
	docker build . --file image/Dockerfile \
	--build-arg VERSION=$(IMAGE_TAG) \
	--build-arg BUILD_DATE=$(IMAGE_ARG_BUILD_DATE) \
	--build-arg GIT_COMMIT=$(IMAGE_ARG_GIT_COMMIT) \
	--tag $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

scan:  ## 🔍️ Scan container image with trivy
	trivy --version
	# Prints full report
	trivy image --exit-code 0 --no-progress $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)
	# Fail on critical vulnerabilities
	trivy image --exit-code 1 --severity CRITICAL --no-progress $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

pull:  ## 📥 Pull container image from registry
	docker pull $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

ci-tag-image:  ## 🏷️  Tag container image to final tag
	docker tag $(IMAGE_REG)/$(IMAGE_REPO):$(CI_IMAGE_TAG) $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

tag-latest:  ## 🏷️  Tag container image as latest
	docker tag $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG) $(IMAGE_REG)/$(IMAGE_REPO):latest

push:  ## 📤 Push container image to registry
	docker push $(IMAGE_REG)/$(IMAGE_REPO):$(IMAGE_TAG)

push-latest:  ## 📤 Push container image to registry (tag: latest)
	docker push $(IMAGE_REG)/$(IMAGE_REPO):latest

run: venv  ## 🏃 Run the server locally using Python & Flask
	. $(SRC_DIR)/.venv/bin/activate \
	&& python src/run.py

test: venv  ## 🎯 Unit tests for Flask app
	. $(SRC_DIR)/.venv/bin/activate \
	&& pytest -v

test-helm:  ## 🎯 Test helm chart
	helm upgrade --install demo ./helm --set service.type=NodePort
	helm test demo

test-report: venv  ## 🎯 Unit tests for Flask app (with report output)
	. $(SRC_DIR)/.venv/bin/activate \
	&& pytest -v --junitxml=test-results.xml

clean:  ## 🧹 Clean up project
	rm -rf $(SRC_DIR)/.venv
	rm -rf tests/node_modules
	rm -rf tests/package*
	rm -rf test-results.xml
	rm -rf $(SRC_DIR)/app/__pycache__
	rm -rf $(SRC_DIR)/app/tests/__pycache__
	rm -rf .pytest_cache
	rm -rf $(SRC_DIR)/.pytest_cache
	k3d cluster delete devCluster

# ============================================================================

venv: $(SRC_DIR)/.venv/touchfile

$(SRC_DIR)/.venv/touchfile: $(SRC_DIR)/requirements.txt
	python3 -m venv $(SRC_DIR)/.venv
	. $(SRC_DIR)/.venv/bin/activate; python3 -m pip install --upgrade pip; pip install -Ur $(SRC_DIR)/requirements.txt
	touch $(SRC_DIR)/.venv/touchfile
