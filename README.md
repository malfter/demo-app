# Demo Web Application

![Logo](./docs/assets/logo.png)

This is a simple Python Flask web application. The app provides system information. Furthermore, the design of the app can be customized with environment variables.

The app has been designed with cloud native demos & containers in mind, in order to provide a real working application for deployment, something more than "hello-world" but with the minimum of pre-requirements. It is not intended as a complete example of a fully functioning architecture or complex software design.

- [Demo Web Application](#demo-web-application)
  - [Building \& Running Locally](#building--running-locally)
    - [Pre-requirements](#pre-requirements)
    - [Quickstart](#quickstart)
    - [Customize App (Environment Variables)](#customize-app-environment-variables)
    - [Makefile](#makefile)
  - [Containers](#containers)
  - [Helm Chart](#helm-chart)
    - [Chart Development](#chart-development)
  - [Thank you](#thank-you)
  - [License](#license)

## Building & Running Locally

### Pre-requirements

- Be using Linux, WSL or MacOS, with bash, make etc
- [Python 3.9+](https://www.python.org/downloads/) - for running locally, linting, running tests etc
- [Docker](https://docs.docker.com/get-docker/) - for running as a container, or image build and push

### Quickstart

    # clone repository
    git clone https://gitlab.com/malfter/demo-app.git

    cd demo-app

    # run app
    make run

### Customize App (Environment Variables)

| Env           | Default    | Description              |
|---------------|------------|--------------------------|
| DEMO_APP_NAME | `Demo App` | Name of the application. |
| DEMO_APP_PORT | `8080`     | Port on which the application can be reached |
| DEMO_APP_BACKGROUND_COLOR  | `darkgreen` | Background and main color of the application. The color can be used to easily identify an application, e.g. when two or more applications are deployed. (Colors: `black`, `blue`, `darkblue`, `cyan`, `darkcyan`, `gray`, `green`, `darkgreen`, `lightgray`, `magenta`, `darkmagenta`, `red`, `darkred`, `white`, `yellow`, `darkyellow`) |

### Makefile

A standard GNU Make file is provided to help with running and building locally.

    > make
    help                 💬 This help message
    local                ☸️  Create local kubernetes environment with k3d
    lint                 🔎 Lint & format, will not fix but sets exit code on error
    lint-fix             📜 Lint & format, will try to fix errors and modify code
    build                🔨 Build container image from Dockerfile
    scan                 🔍️ Scan container image with trivy
    pull                 📥 Pull container image from registry
    ci-tag-image         🏷️  Tag container image to final tag
    tag-latest           🏷️  Tag container image as latest
    push                 📤 Push container image to registry
    push-latest          📤 Push container image to registry (tag: latest)
    run                  🏃 Run the server locally using Python & Flask
    test                 🎯 Unit tests for Flask app
    test-helm            🎯 Test helm chart
    test-report          🎯 Unit tests for Flask app (with report output)
    clean                🧹 Clean up project

Make file variables and default values, pass these in when calling `make`, e.g. `make build IMAGE_REPO=foo/bar`

| Makefile Variable | Default                |
| ----------------- | ---------------------- |
| IMAGE_REG         | `registry.gitlab.com`  |
| IMAGE_REPO        | `malfter/demo-app`     |
| IMAGE_TAG         | `latest`               |

The app runs under Flask and listens on port `8080` by default, this can be changed with the `DEMO_APP_PORT` environmental variable.

## Containers

Public container image is [available on GitLab Container Registry](https://gitlab.com/malfter/demo-app/container_registry)

Run in a container with:

    docker run --rm -it -p 8080:8080 registry.gitlab.com/malfter/demo-app:latest

Should you want to build your own container, use `make build` and the above variables to customize the name & tag.

## Helm Chart

To install the latest version of the chart, use the following command:

    helm repo add malfter https://gitlab.com/api/v4/projects/30659108/packages/helm/stable
    helm install my-demo malfter/demo-app

If the repository has previously been added, you may need to run:

    helm repo update

### Chart Development

Install helm chart:

    helm install demo-app ./helm --set service.type=NodePort

In order to use the same command when installing and upgrading a release, use the following command:

    helm upgrade --install demo ./helm --set service.type=NodePort

## Thank you

This project based on [benc-uk/python-demoapp](https://github.com/benc-uk/python-demoapp).

Thank you for your work!

## License

[MIT License](./LICENSE)

---

Icons made by [Freepik](https://www.freepik.com) from [Flaticon](https://www.flaticon.com)
