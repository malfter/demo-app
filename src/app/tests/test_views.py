def test_home(client):
    resp = client.get("/")

    assert resp.status_code == 200
    assert b"Demo App" in resp.data


def test_home_content(client):
    resp = client.get("/")

    assert resp.status_code == 200
    assert b"Alfter" in resp.data


def test_info(client):
    resp = client.get("/info")

    assert resp.status_code == 200
    assert b"System Informations" in resp.data


def test_help(client):
    resp = client.get("/help")

    assert resp.status_code == 200
    assert b"To customize" in resp.data


def test_echo_GET(client):
    resp = client.get("/echo")

    assert resp.status_code == 200
    assert b"GET" in resp.data


def test_echo_POST_text_plain(client):
    resp = client.post("/echo", data=dict(key="value"), content_type="text/plain")

    assert resp.status_code == 200
    assert b"POST" in resp.data


def test_echo_POST_application_json(client):
    resp = client.post("/echo", data="{}", content_type="application/json")

    assert resp.status_code == 200
    assert b"{}" in resp.data


def test_echo_POST_unknown_contenttype(client):
    resp = client.post("/echo", data="{}", content_type="unknown/contenttype")

    assert resp.status_code == 415


def test_echo_PATCH(client):
    resp = client.patch("/echo")

    assert resp.status_code == 200
    assert b"PATCH" in resp.data


def test_echo_PUT(client):
    resp = client.put("/echo")

    assert resp.status_code == 200
    assert b"PUT" in resp.data


def test_echo_DELETE(client):
    resp = client.delete("/echo")

    assert resp.status_code == 200
    assert b"DELETE" in resp.data
