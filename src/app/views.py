from flask import json, render_template, request, Response, current_app as app

import cpuinfo
import datetime
import os
import platform
import psutil


@app.route("/")
def index():
    appinfo = get_app_info()

    return render_template("index.html", app=appinfo)


@app.route("/info")
def info():
    appinfo = get_app_info()

    osinfo = {}
    osinfo["plat"] = platform
    osinfo["cpu"] = cpuinfo.get_cpu_info()
    osinfo["mem"] = psutil.virtual_memory()
    osinfo["net"] = psutil.net_if_addrs()
    osinfo["boottime"] = datetime.datetime.fromtimestamp(psutil.boot_time()).strftime(
        "%Y-%m-%d %H:%M:%S"
    )

    return render_template("info.html", app=appinfo, info=osinfo)


@app.route("/help")
def help():
    appinfo = get_app_info()

    return render_template("help.html", app=appinfo)


def get_app_info():
    appinfo = {}
    appinfo["name"] = os.environ.get("DEMO_APP_NAME", default="Demo App")
    appinfo["port"] = os.environ.get("DEMO_APP_PORT", 8080)
    appinfo["color"] = os.environ.get("DEMO_APP_BACKGROUND_COLOR", default="darkgreen")
    appinfo["build_rev"] = os.environ.get("DEMO_APP_BUILD_REV", default="UNKNOWN")
    appinfo["build_date"] = os.environ.get(
        "DEMO_APP_BUILD_DATE", default="UNKNOWN"
    ).split("T")[0]
    return appinfo


@app.route("/echo", methods=["GET", "POST", "PATCH", "PUT", "DELETE"])
def api_echo():
    if request.method == "GET":
        if is_from_browser(request.user_agent):
            appinfo = get_app_info()
            echoinfo = {}
            echoinfo["msg"] = "ECHO: GET\n"
            return render_template("echo.html", app=appinfo, echo=echoinfo)

        else:
            msg = "ECHO: GET\n"
            return Response(msg, status=200, mimetype="text/plain")

    elif request.method == "POST":
        if request.headers["Content-Type"] == "text/plain":
            msg = "ECHO: POST\n\nText Message: " + str(request.data)
            return Response(msg, status=200, mimetype="text/plain")

        elif request.headers["Content-Type"] == "application/json":
            msg = json.dumps(request.json)
            return Response(msg, status=200, mimetype="application/json")

        else:
            msg = "415 Unsupported Media Type ;)"
            return Response(msg, status=415, mimetype="text/plain")

    elif request.method == "PATCH":
        msg = "ECHO: PATCH\n"
        return Response(msg, status=200, mimetype="text/plain")

    elif request.method == "PUT":
        msg = "ECHO: PUT\n"
        return Response(msg, status=200, mimetype="text/plain")

    elif request.method == "DELETE":
        msg = "ECHO: DELETE"
        return Response(msg, status=200, mimetype="text/plain")


def is_from_browser(user_agent):
    return user_agent.browser in [
        "camino",
        "chrome",
        "firefox",
        "galeon",
        "kmeleon",
        "konqueror",
        "links",
        "lynx",
        "msie",
        "msn",
        "netscape",
        "opera",
        "safari",
        "seamonkey",
        "webkit",
    ]
